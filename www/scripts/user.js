var config = require('./conf/config.json');
var md5 = require('blueimp-md5').md5;
var url = require('url');
var ls = require('./localstorage.js');
var ApiClient = require('./ApiClient.js');

var apiClient = new ApiClient(config.apiRoot);

module.exports.displayCurrent = function () {
  return apiClient.get('users', 'current').then(function (user) {
    ls.put('currentUser', user);
    return user;
    
  }).fail(function () {
    ls.put('currentUser', null);

  }).always(function(user){
    $('.userInfo').html(
      templates['user'].render({
        config: config,
        loginUrl: module.exports.getLoginUrl(),
        user: user,
        gravatarHash: module.exports.generateGravatarHash()
      })
    );
    
  });
}

module.exports.refresh = module.exports.displayCurrent;

module.exports.getLoginUrl = function () {
  return config.apiRoot + '/auth?redirectUrl=' + encodeURIComponent(window.location.href);
}

module.exports.generateGravatarHash = function(){
  var user = module.exports.getCurrent();
  if (!user) {
    return null;
  }
  return md5(user.email || user.username);
}

module.exports.getCurrent = function(){
  return ls.get('currentUser');
}
