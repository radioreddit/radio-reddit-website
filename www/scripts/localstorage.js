module.exports.get = function(key){
  var value = localStorage.getItem(key);
  return JSON.parse(value);
}

module.exports.put = function(key,value){
  var valueAsString = JSON.stringify(value);
  localStorage.setItem(key,valueAsString);
}

module.exports.remove = function(key){
  localStorage.removeItem(key);
};