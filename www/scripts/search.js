var utils = require('./utils.js');
var config = require('./conf/config.json');

module.exports.init = function(){
  $('form[name="search"]').on('submit', function (e) {
    e.preventDefault();
    window.location.href = ('#!/library/search/' + encodeURIComponent($(this).children('[name="q"]').val().trim()));
  });
}
module.exports.setValue = function(s){
  $('input[name="q"]').val(s);
}
module.exports.getValue = function(){
  return $('input[name="q"]').val();
}