module.exports.peak = function(ar){
  var x = ar.pop();
  ar.push(x);
  return x;
};
module.exports.contains = function (ar, value){
  for(var i = 0; i < ar.length; i++){
    if(ar[i] == value){
      return true;
    }else if ( module.exports.isObjectWithId(ar[i]) && module.exports.isObjectWithId(value) && ar[i].id == value.id){
      return true;
    }
  }
  return false;
}

module.exports.isObjectWithId = function(o){
  return ( typeof o == 'object' && o.id !== undefined);
}

module.exports.createPath = function(){
  var path = '#!/';
  for (var i = 0; i < arguments.length; i++) {
    if('object' === typeof arguments[i] && 'undefined' !== arguments[i].namespace){
      path += encodeURIComponent(arguments[i].namespace);
    }else{
      path += encodeURIComponent(arguments[i]);
    }
    if(i + 1 != arguments.length){
      path += '/'
    }
  }
  return path;
};
module.exports.parseQueryString = function (qstr, key){
  var query = qstr.substring(qstr.indexOf('?')+1);
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    if (decodeURIComponent(pair[0]) == key) {
        return decodeURIComponent(pair[1]);
    }
  }

}
module.exports.toggleDefaultView = function(changeTo){
  var defaultView = $('.viewtoggle.default');
  var notDefaultView = $('.viewtoggle:not(.default)');
  if(defaultView.data('view') != changeTo){
    defaultView.removeClass('default');
    notDefaultView.addClass('default');
  }
}
module.exports.triggerHashBang = function () {
  // Parse path
  var hashBangPath = (location.hash.indexOf('#!/') > -1) ? location.hash.substr(3) : '';
  var pathAr = hashBangPath.split('?')[0].split('/');
  for (var x=0; x<pathAr.length; x++) {
    pathAr[x] = decodeURIComponent(pathAr[x]);
  }

  // Default route
  if (!pathAr.length || (pathAr.length === 1 && !pathAr[0])) {
    pathAr.shift();
    pathAr.push('channels');
  }

  // Call controller with parameters split from path
  $(document).trigger('rr.'+pathAr.shift(), pathAr);
}

module.exports.navigateTo = function(path){
  var pathAr = path.replace(/#!\//,'').split('/');
  $(document).trigger('rr.'+pathAr.shift(),pathAr);  
}