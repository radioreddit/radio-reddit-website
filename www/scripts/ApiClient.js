module.exports = function ApiClient (apiRoot) {
  function request (method) {
    var args = Array.prototype.slice.call(arguments, 1);
    var data;
    if (typeof args[args.length-1] === 'object') {
      data = args.pop();
    }
    $.each(args, function (index, arg) {
      args[index] = encodeURIComponent(arg);
    });
    
    var ajaxParameters = {
      url: apiRoot + '/' + args.join('/'),
      data: data,
      xhrFields: {
        withCredentials: true
      },
      method: method
    };
    
    if (ajaxParameters.method !== 'GET') {
      ajaxParameters.data = JSON.stringify(ajaxParameters.data);
      ajaxParameters.contentType = 'application/json';
    }
    
    return $.ajax(ajaxParameters);
  }
  
  this.get = function () {
    return request.apply(this, ['GET'].concat(Array.prototype.slice.call(arguments)))
  };
  this.put = function () {
    return request.apply(this, ['PUT'].concat(Array.prototype.slice.call(arguments)))
  };
  this.post = function () {
    return request.apply(this, ['POST'].concat(Array.prototype.slice.call(arguments)))
  };
  this.delete = function () {
    return request.apply(this, ['DELETE'].concat(Array.prototype.slice.call(arguments)))
  };
  
  
}