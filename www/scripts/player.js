var config = require('./conf/config.json');
var url = require('url');
var user = require('./user');

var instance = null;
var metaInterval = null;
module.exports.init = function () {
  instance = $('<div>').jPlayer({
    ready: function () {
     
    },
    swfPath: '/js',
    supplied: 'mp3',
    autoBlur: false,
    keyEnabled: true
});
}

function setUrl(url) {
  instance.jPlayer('setMedia', {mp3: url});
  instance.jPlayer('play');
  $('[data-action="resume"]').hide();
  $('[data-action="pause"]').show();
}

function displayMetaData(primary, secondary, artUrl) {
  $('footer .artist, footer .song').hide();
  if (primary) {
    $('footer .artist').text(primary).slideDown();
  }
  if (secondary) {
    $('footer .song').text(secondary).slideDown();
  }
  if (artUrl) {
    $('footer img.art').attr('src', artUrl);
  }
}

module.exports.updateChannelMetadata = function updateChannelMetadata () {
  var $footer = $('footer[data-media-type="channel"]');
  if (!$footer.length) {
    return;
  }

  $.get([
    config.apiRoot,
    'channels',
    encodeURIComponent($footer.attr('data-media-id')),
    'metadata'
  ].join('/')).done(function (metadata) {
    if (metadata[0]) {
      var newLabelText = (metadata[0].artist ? (metadata[0].artist + ' - ') : '') + metadata[0].title;
      var $footerSong = $('footer .song');
      
      if ($footerSong.text() !== newLabelText) {
        $footerSong.hide().text(newLabelText).slideDown();
      }
    }
  });
};

setInterval(module.exports.updateChannelMetadata, 5000);

$(function () {
  $(document).on('keypress', function (e) { 
    if (e.keyCode === 32 && e.target === $('body')[0] && module.exports.getStatus().src) {
      e.preventDefault();
      (module.exports.getStatus().paused ? module.exports.resume : module.exports.pause)();
    }
  });
});

module.exports.loadVotes = function loadVotes (trackId) {
  var $vote = $('footer .vote');
  $vote.find('.score').text('-');
  $('[data-action="voteUp"], [data-action="voteDown"]').removeClass('active');
  
  $.get([
     config.apiRoot,
     'tracks',
     encodeURIComponent(trackId),
     'post'
   ].join('/')).done(function (post) {
     console.log(post);
     $vote.find('.score').text(post.score);
     $vote.show();
   }).fail(function () {
     $vote.hide();
   });
  
  var userId = user.getCurrent().id;
  if (userId) {
    $.ajax({
      url: [
        config.apiRoot,
        'votes',
        'users',
        encodeURIComponent(userId),
        'tracks',
        encodeURIComponent(trackId)
     ].join('/'),
     xhrFields: {
       withCredentials: true
     }
   }).done(function (votes) {
     if (votes.length) {
       $('[data-action="voteUp"]').toggleClass('active', votes[0].isUpVote);
       $('[data-action="voteDown"]').toggleClass('active', !votes[0].isUpVote);
     }
   });
  }
};

module.exports.pause = function pause () {
  instance.jPlayer('pause');
  $('[data-action="pause"]').hide();
  $('[data-action="resume"]').show();
}

module.exports.resume = function resume() {
  instance.jPlayer('play');
  $('[data-action="resume"]').hide();
  $('[data-action="pause"]').show();
}

module.exports.getStatus = function getStatus() {
  return instance.data().jPlayer.status;
}

module.exports.play = function play (id, type) {
  $('footer')
    .attr('data-media-type', type)
    .attr('data-media-id', id);
  
  switch (type) {
  case 'track':
    $.get([
      config.apiRoot,
      'tracks',
      encodeURIComponent(id)
    ].join('/')).done(function (track) {
      console.log(track);
      displayMetaData(
        track.Artist.name,
        track.title,
        'images/channels/main.png'
      );
      setUrl(track.media.mp3High);
    });
    
    module.exports.loadVotes(id);
    
    break;
    
  case 'channel':
    $('footer .vote').hide();
    $.get([
      config.apiRoot,
      'channels',
      encodeURIComponent(id)
    ].join('/')).done(function (channel) {
      displayMetaData(
        channel.name + ' Channel',
        channel.description || '',
        'images/channels/' + encodeURIComponent(channel.name.toLowerCase().replace(/ /g,'')) + '.png'
      );
      
      module.exports.updateChannelMetadata();
      
      var streamToPlay = null;
      $.each(channel.Streams, function (index, stream) {
        if (stream.contentType === 'audio/mpeg') {
          if (!streamToPlay || streamToPlay.averageBitrate < stream.averageBitrate) {
            streamToPlay = stream;
          }
        }
      });
      
      setUrl(streamToPlay.url);
      
    });
    break;
    
  }
}

module.exports.updateMetadata = function(id,type){
  var getUrl = {
    protocol: location.protocol,
    host: config.apiRoot+'/'+encodeURIComponent(type)+'/'+encodeURIComponent(id)
  };
  $.get(url.format(getUrl)).done(function(data){
    $(".jp-title").empty();
    $(".jp-title").append(module.exports.determineTitle(data));
  });
}

$(function () {
  displayMetaData('Welcome to Radio Reddit!', 'Choose a channel or check out our library of music by Redditors.', '/images/channels/main.png');
});