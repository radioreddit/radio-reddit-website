var config = require('./conf/config.json');
var imagesLoaded = require('imagesloaded');
var ls = require('./localstorage.js');
var player = require('./player.js');
var playlist = require('./playlist.js');
var qs = require('querystring');
var search = require('./search.js');
var url = require('url');
var user = require('./user.js');
var utils = require('./utils');
var ApiClient = require('./ApiClient.js');

var apiClient = new ApiClient(config.apiRoot);

// Static Pages
$.each(['home'], function (index, controllerName) {
  module.exports[controllerName] = function (e) {
    $('#main').html(templates[controllerName].render());
  }
});

module.exports.about = function about (e, templateName) {
  templateName = templateName || 'about';
  $('#main').html(templates[templateName].render());
}

module.exports.channels = function channels (e, channelId) {
  if (!parseInt(channelId)) {
    // Show the list of channels
    return apiClient.get('channels').then(function (channels) {
      $.each(channels, function (index, channel) {
        channel.image = 'images/channels/' + encodeURIComponent(channel.name.toLowerCase().replace(/ /g,'')) + '.png';
        channel.url = '#!/channels/' + encodeURIComponent(channel.id);
      });
      
      $('#main').html(
        templates['channelList'].render({
          channels: channels
        })
      );
      
      masonry();
      
    }).fail(errorHandler);
  
  
  } else {
    // Show a specific channel
    return $.when.apply(this, [
      apiClient.get('channels', channelId),
      apiClient.get('channels', channelId, 'metadata')
      
    ]).then(function (channelRes, metadataRes) {
      $('#main').html(
        templates['channel'].render({
          channel: channelRes[0],
          metadata: metadataRes[0]
        })
      );
    }).fail(errorHandler);
    
  }
  
}

module.exports.hot = function hot (e){
  apiClient.get('tracks', 'hot').then(function (tracks){
    $('#main').html(
      templates['librarySearch'].render({
        tracks: tracks,
        title: 'Hot',
        description: 'The latest tracks trending on <a href="http://www.reddit.com/r/radioreddit">/r/radioreddit</a>'
      })
    );
  }).fail(errorHandler);;
};

module.exports.library = function library (e, method) {
  switch (method) {
    case 'search':
      var q = arguments[2];
      $('form[name="search"] input[name="q"]').val(q);
      var page = (arguments[3]) ? arguments[3] : 1;
      apiClient.get('tracks', {
        any: q,
        page: (page - 1)
      }).then(function (tracks) {
        $('#main').html(
          templates['librarySearch'].render({
            tracks: tracks,
            q: q,
            showPrevNext: true
          })
        );
        wirePrevNextButtons(page, ['library', 'search', q].join('/') + '/');
      }).fail(errorHandler);
      break;
    case 'tracks':
      var q = arguments[2];
      apiClient.get('tracks', q
      ).then(function (tracks) {
        $('#main').html(
          templates['librarySearch'].render({
            tracks: [tracks],
            title: tracks.title
          })
        );
      }).fail(errorHandler);
      break;
    default:
      var page = (arguments[1]) ? arguments[1] : 1;
      apiClient.get('tracks',{
        page: (page - 1)
      }).then(function (tracks){
        if(tracks && tracks.length > 0){
          $('#main').html(
            templates['librarySearch'].render({
              tracks: tracks,
              showPrevNext: true
            })
          );
          wirePrevNextButtons(page);
        }else{
          //utils.navigateTo('library/');
        }
      }).fail(errorHandler);
    
  }
}

function wirePrevNextButtons(page, path){
  var navigateToPath = (path) ? path : 'library/';
  ls.put('page',page);
  $('.next').click(function (e){
    var currentPage = ls.get('page');
    var nextPage = parseInt(currentPage) + 1;
    utils.navigateTo(navigateToPath + nextPage);
  });
  
  $('.prev').click(function (e){
    var currentPage = ls.get('page');
    var prevPage = parseInt(currentPage) - 1;
    if(prevPage > 0){
      utils.navigateTo(navigateToPath + prevPage);
    }
  });
}

function masonry() {
  var $container = $('.listview');
  $container.find('.listitem').addClass('gridview');
  $container.imagesLoaded(function () {
    $container.masonry({
      columnWidth: 10,
      itemSelector: '.listitem'
    });
    $container.data('masonryActive', 'true');
  });
}

module.exports.artist= function(e,id){
  $.when(
    apiClient.get('artists', id),
    apiClient.get('albums', {ArtistId: id})
  ).then(function(artistObject,albumsObject) {
    var artist = artistObject[0];
    artist.Albums = albumsObject[0];
    artist.editable = determineIfUserOwnsArtist(artist);
    var newHtml = templates['artist'].render(artist);
    $('#main').html(newHtml);
    wireEditArtistForm();
  }).fail(errorHandler);
};

function determineIfUserOwnsArtist(artist){
  var currentUser = user.getCurrent();
  var retVal = false;
  if(!currentUser){
    return retVal;
  }
  
  for(var i = 0; i < currentUser.Artists.length; i++){
    if(currentUser.Artists[i].id == artist.id){
      retVal = true;
      break;
    }
  }
  return retVal;
}

module.exports.dmca = function(e){
  var rendered = templats['dmca'].render();
  $('#main').html(rendered);
}
module.exports.termsOfUse = function(e){
  var rendered = templates['termsofuse'].render();
  $('#main').html(rendered);
};
module.exports.addToPlaylist = function(e, id){
  apiClient.get('playlists', {userid: user.getCurrent().id}).then(function(playlists){
    var data = {};
    data.playlists = playlists;
    data.trackId = encodeURIComponent(id);
    var rendered = templates['add_to_playlist'].render(data);
    $('#main').html(rendered);
    $('#add_to_playlist_form').submit(addToPlaylistSubmit);
  });
}
function addToPlaylistSubmit (e){
  e.preventDefault();
  var dataArr = $(this).serializeArray();
  var data = {};
  dataArr.forEach(function(element, index, array){
    data[element.name] = encodeURIComponent(element.value);
  });
  if(data.playlistSelect === 'create'){
    $.when(playlist.createPlaylist(data)).then(function(result){
      playlist.addToPlaylist(result.id,data.trackId);
      utils.navigateTo('playlists/'+encodeURIComponent(result.id));
    });
  }else{
    playlist.addToPlaylist(data.playlistSelect,data.trackId);
    utils.navigateTo('playlists/'+encodeURIComponent(data.playlistSelect));
  }
}
module.exports.playlists = function(e, id){
  apiClient.get('playlists', {
    userid: user.getCurrent().id
  }).then(function (playlists) {
    var rendered = templates['playlists'].render(playlists);
    $('#main').html(rendered);
    if(id){
      var playlistTracks = {};
      //TODO rework via issue #14
      
    }
  });
}

module.exports.profile = function (e){

  var userInfo = user.getCurrent();
  userInfo.gravatarHash = user.generateGravatarHash();
  var rendered = templates['userprofile'].render(userInfo);
  $('#main').html(rendered);
  $('.userProfileForm').submit(function (e){
    e.preventDefault();
    var formData = {
      firstName: $('#firstName').val(), 
      lastName: $('#lastName').val(),
      email: $('#email').val() 
    };
    apiClient.put('users', user.getCurrent().id, formData)
    .then(function (result){
      if(result && result.username){
        ls.put('currentUser', result);
      }
    }).fail(errorHandler);
  });
  
  $('.edit fieldset').hide();
  
  $('.edit a').click(function (e){
    e.preventDefault();
    $('.edit fieldset').toggle();
  });
  
  
    
}
module.exports.upload = function (e,id){
  return user.refresh().always(function () {
    if ( !(user.getCurrent() && user.getCurrent().id) ) {
      return $('#main').html(
        templates['error'].render({
          401: true
        })
      );
    }
    
    var data = {};
    
    data.artists = user.getCurrent().Artists;
    
    var albumPromises = [];
    $(data.artists).each(function (index, artist) {
      albumPromises.push(
        apiClient.get(
          'albums', 
          {
            ArtistId: artist.id
          }
        )
      );
    });
    $.when.apply($, albumPromises).then(function () {
      if (!albumPromises.length) {
        return;
      }
      for (var x=0; x<albumPromises.length; x++) {
        data.artists[x].Albums = arguments[x][0] || [];
      }
    }).then(function () {
      $('#main').html( templates['upload'].render(data) );
    });
  });
}

function determineArtist(id){
  var artists = user.getCurrent().Artists;
  for(var i = 0; i < artists.length; i++){
    if (artists[i].id == id){
      return artists[i];
    }
  }
}
function wireCreateArtistForm() {
  $('.createArtist').click(function (e) {
    e.preventDefault();

    var formData = {};
    $.each($(this).closest('form').serializeArray(), function(index, kv) {
      formData[kv.name] = kv.value || null;
    });
    apiClient.post('artists', formData).then(function (result){
      return user.displayCurrent().then(function (){
        utils.navigateTo('upload');
      });
    }).fail(function (err) {
      alert('Could not create artist.  Please ensure you have filled in all fields correctly.');
    });
  });
}
function wireEditArtistForm(){
  $('.edit fieldset').hide();
  $('.edit a').click(function (e){
    e.preventDefault();
    $('.edit fieldset').toggle();
  });
  $('.updateArtist').click(function (e){
    e.preventDefault();
    var form = $(this).closest('form');
    var artistId = form.find('input[name="ArtistId"]').val();
      var formData = {
        name: form.find('input[name="name"]').val(),
        description: form.find('textarea[name="description"]').val(),
        homeCity: form.find('input[name="homeCity"]').val(),
        homeStateProvince: form.find('input[name="homeStateProvince"]').val(),
        homeCountry: form.find('input[name="homeCountry"]').val(),
        websiteUrl: form.find('input[name="websiteUrl"]').val(),
        googleAnalyticsPropertyId: 'UA-88888888-1'
      }
      apiClient.put('artists', artistId, formData).then(function (result){
        utils.navigateTo('upload');
      }).fail(errorHandler);
  });

}
function wireEditAlbumForm(){
  $('.edit fieldset').hide();
  $('.edit a').click(function (e){
    e.preventDefault();
    $('.edit fieldset').toggle();
    $('#albumTitle span').toggle();
    $('.description').toggle();    
  });
  $('.updateAlbum').click(function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    var albumId = form.find('input[name="AlbumId"]').val();
    var formData = {
      title: form.find('input[name="title"]').val(),
      description: form.find('textarea[name="description"]').val()
    }
    apiClient.put('albums',albumId, formData).then(function (result){
      utils.navigateTo('upload/'+result.id);
    }).fail(errorHandler);;
  });
}

function wireCreateAlbumForm(){
  fileToUpload = {};
  wireFormDragAndDrop(fileToUpload);
  $('.createAlbum').click(function (e){
    e.preventDefault();
    var form = $(this).closest('form');
    var artistId = form.find('input[name="ArtistId"]').val();
    var coverImage = (fileToUpload.file) ? fileToUpload.file.name : '';
    var formData = {
      title: form.find('input[name="title"]').val(),
      description: form.find('textarea[name="description"]').val(),
      coverImage: coverImage
    }
    apiClient.post('artists',artistId, 'albums', formData).then(function (result){
      if(fileToUpload.file){
        performS3UploadalbumArt(form,fileToUpload,result.id,'upload/'+result.id);
      }else{
        utils.navigateTo('upload/'+result.id);
      }
    }).fail(errorHandler);
  });
}
function wireUploadForm(){
  fileToUpload = {};
  wireFormDragAndDrop(fileToUpload);
  $('.s3UploadButton').click(function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    var formData = {
      title: form.find('input[name="title"]').val(), 
      ArtistId: form.find('input[name="ArtistId"]').val(),
      AlbumId: form.find('input[name="AlbumId"]').val(),
      UserId: form.find('input[name="UserId"]').val() 
    };
    apiClient.post('albums', formData.AlbumId, 'tracks', formData).then(function (result) {
      var redditData = {
        iden: form.find('input[name="iden"]').val(),
        captcha: form.find('input[name="captcha"]').val(),
        title: form.find('input[name="title"]').val() + ' by ' + $('#albumTitle').text(),
        url: [location.protocol+'/',location.hostname,'#!','library/tracks',result.id].join('/'),
        trackId: result.id
      }
      apiClient.post('tracks','posts', redditData).then(function (redditResult){
        performS3UploadTrack(form, fileToUpload, result.id, location.hash.substring(3));
      });
    }).fail(errorHandler);
  });
}

function performS3UploadalbumArt(form, fileToUpload, albumId, redirectTo){
  performS3Upload(form, fileToUpload, ['art/album',albumId,fileToUpload.file.name].join('/'), redirectTo, ['albums',albumId,'artUploadFormData']);
}

function performS3UploadTrack(form, fileToUpload, trackId, redirectTo){
  performS3Upload(form, fileToUpload, ['tracks',trackId,'original'].join('/'), redirectTo, ['tracks',trackId,'uploadFormData']);
}

function performS3Upload(form, fileToUpload, key, redirectTo, configPathParts){
  apiClient.get.apply(this, configPathParts).then(function (s3Config){
    var awsData = new FormData();
    awsData.append('key',  key);
    awsData.append('AWSAccessKeyId', s3Config.key);
    awsData.append('acl', 'private');
    awsData.append('policy', s3Config.policy);
    awsData.append('signature', s3Config.signature);
    awsData.append('Content-Type', form.find('input[name="Content-Type"]').val());
    awsData.append('file', fileToUpload.file);
          
    $.ajax({
      url: 'http://'+s3Config.bucket+'.s3.amazonaws.com/',
      method: 'POST',
      data: awsData,
      contentType: false,
      processData: false,
      crossDomain: true
    }).then(function (awsResult){
      utils.navigateTo(redirectTo);
    }).fail(errorHandler);
  }).fail(errorHandler);
}
function wireFormDragAndDrop(fileToUpload){
  $('.dragndrop').on('dragover', function(e){
    $(this).addClass('hover');
  });
  $('.dragndrop').on('dragleave', function(e){
    $(this).removeClass('hover');
  });
  $('.dragndrop').on('drop', function(e){
    e.preventDefault();
    e.stopPropagation();
    $(this).removeClass('hover');
    fileToUpload.file = e.originalEvent.dataTransfer.files[0];
    $(this).find('input').hide();
    $(this).append(fileToUpload.file.name);
    $(this).closest('form').find('input[name="Content-Type"]').val(fileToUpload.file.type);
  });
  $('.dragndrop input[type="file"]').on('change', function(e) {
    fileToUpload.file = e.target.files[0];
    $(this).hide();
    $(this).closest('.dragndrop').append(fileToUpload.file.name);
    $(this).closest('form').find('input[name="Content-Type"]').val(fileToUpload.file.type);
  });
  
}
module.exports.approvalList = function(e){
  apiClient.get('tracks','unapproved').then(function (result){
    var rendered = templates['approvalList'].render({
      tracks: result
    });
    $('#main').html(rendered);

    $('.approve').click(function(e){
      $('table input:checked').each(function (){
        var row = $(this).closest('tr');
        var trackId = $(this).val();
        apiClient.post('tracks','approve', {trackId: trackId}).then(function (result){
          row.remove();
        }).fail(errorHandler);
      });
    });
    $('.deny').click(function(e){
      $('table input:checked').each(function (){
        var row = $(this).closest('tr');
        var trackId = $(this).val();
        apiClient.post('tracks','deny', {trackId: trackId}).then(function (result){
          row.remove();
        }).fail(errorHandler);
      });
    });
  });
}
module.exports.logout = function (e){
  apiClient.post('auth','logout').always(function(result){
    user.displayCurrent();
    utils.navigateTo('channels');
  });
}

function errorHandler(e){
  var rendered = templates['error'].render();
  $('#main').html(rendered);
}
