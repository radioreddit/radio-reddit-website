var config = require('./conf/config.json');
var url = require('url');
var utils = require('./utils');
var user = require('./user');
module.exports.createPlaylist = function(data){
  var postUrl = {
    host: config.apiRoot+'/playlists',
  };
  var playlist = {};
  playlist.title = (data.createNewTitle) ? data.createNewTitle : 'My Playlist';
  playlist.UserId = user.getCurrent().id;
  playlist.isPublic = false;
  return $.ajax(
    {
      type: 'POST',
      url: url.format(postUrl),
      data: playlist
    }
  ).done(function(result){

  }).fail(function(e){

  });  

}
module.exports.addToPlaylist = function(playlistId,trackId){
  var getPlaylistUrl = {
    host: config.apiRoot+'/playlistsTracks',
  };
  var playlist = {
    'PlaylistId': encodeURIComponent(playlistId),
    'TrackId': encodeURIComponent(trackId)
  }
  $.ajax(
    {
      type: 'POST',
      url: url.format(getPlaylistUrl),
      data: playlist
    }
  ).done(function(result){
  
  });
}

module.exports.removeFromPlaylist = function(playlistId,trackId){
  var getPlaylistUrl = {
    host: [
      config.apiRoot,
      'playlistsTracks',
      encodeURIComponent(playlistId),
      encodeURIComponent(trackId)
    ].join('/')
  };
  
  $.ajax(
    {
      type: 'DELETE',
      url: url.format(getPlaylistUrl),
      data: {
        PlaylistId: playlistId,
        TrackId: trackId
      }
    }
  ).done(function(result){

  });
}