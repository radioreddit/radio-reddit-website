var config = require('./conf/config.json');
var controllers = require('./controllers.js');
var userjs = require('./user.js');
var utils = require('./utils.js');
var search = require('./search.js');
var player = require('./player.js');
var url = require('url');
var playlist = require('./playlist.js');

module.exports.wire = function(){
  window.addEventListener('popstate', popstate);
  $.each(controllers, function (key, controllerFunc) {
    $(document).on('rr.' + key, function(e) {
      controllerFunc.apply(this, arguments);
      var data = {
        namespace: e.namespace,
        args: [].slice.call(arguments, 1)
      };
      var loc = 'http://' + location.host + utils.createPath.apply(this, arguments);
      history.pushState(data, null, loc);
      ga('send', 'pageview', utils.createPath.apply(this, arguments) + data.querystring);
      
      // Set up navigation styling
      $('nav a').removeClass('active');
      $('nav a[data-controller="' + key + '"]').addClass('active');
      window.scrollTo(0,0);
    });
  });
  
  $('.viewtoggle').click(viewtoggle);
  
  $('.breadSandwich').click(function (e) {
    $('#sidebar').toggleClass('show');
  });
  
  $('.searchButton').click(function (e) {
    var $searchSection = $('.searchSection');
    $searchSection.toggleClass('show');
    if ($searchSection.is(':visible')) {
      $searchSection.find('input').focus().select();
    }
  });

  $('#main').on('click', '.removeFromPlaylist', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var playlistId = $(this).data('playlistid');
    playlist.removeFromPlaylist(playlistId,id);
    $(this).closest('.listitem').remove();
  });
  
  //TODO change to a onhash listener
  $('#main').on('click', 'a[href*=#\\!]', function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
    utils.navigateTo(href);
  });
  
};

function popstate (e){
  if('undefined' !== e.state && null !== e.state && 'undefined' !== e.state.namespace){
    var data = e.state;
    data.args.unshift(e);
    controllers[data.namespace].apply(this,data.args);
  } else if (e.state === null) {
    search.setValue(utils.parseQueryString(location.hash,'q'));
    utils.triggerHashBang();
  }
}

function viewtoggle (){
  var viewToggleType = $(this).data('view');
  $(document).trigger('rr.'+viewToggleType);
};

