var ApiClient = require('./ApiClient.js');
var config = require('./conf/config.json');
var qs = require('querystring');
var url = require('url');
var controllers = require('./controllers.js');
var utils = require('./utils');
var search = require('./search.js');
var player = require('./player.js');
var user = require('./user.js');
var listeners = require('./listeners.js');

var apiClient = new ApiClient(config.apiRoot);

function pollForTrackStatus(trackId) {
  var dfd = $.Deferred();
  
  function poll(trackId) {
    apiClient.get('tracks', trackId).then(function (track) {
      switch (track.state) {
      case 'Approved':
        dfd.resolve();
        break;
        
      case 'Rejected':
      case 'DMCATakedown':
        dfd.reject();
        break;
        
      case 'Created':
      case 'Uploaded':
      default:
        return $.Deferred().reject().promise(); // "fail" to try again in 5 seconds
      }
    }).fail(function (e) {
      setTimeout(function () {
        poll(trackId);
      }, 5000);
    });
  }
  
  poll(trackId);
  
  return dfd.promise();
}

$(function() {
  listeners.wire();
  player.init();
  search.init();
  user.displayCurrent()
  utils.triggerHashBang();
  
  $('[data-href-prepend-api-root="true"]').each(function (index, el) {
    var $el = $(el);
    $el.attr('href', config.apiRoot + $el.attr('href'));
  });
  
  function onTrackFieldBlur() {
    var $this = $(this);
    var $row = $this.closest('tr');
    var trackId = parseInt($row.attr('data-track-id'));
    
    apiClient.put('tracks', trackId, {
      title: $row.find('[name="title"]').val(),
      allowDownload: $row.find('[name="allowDownload"]').is(':checked'),
      allowRelease: $row.find('[name="allowRelease"]').is(':checked')
    }).then(function () {
      // TODO: Do something here... check box that fades away?  Green outline?  UX for this needs decided on and standardized site-wide.
    }).fail(function (e) {
      alert('Could not save track title.  Please try again at another time or contact us for assistance.');
    });
  }
  $('body').on('blur', '.artistManage table.tracks tr input[type="text"]', onTrackFieldBlur); // TODO: Also fire on slow (3s?) debounced change/keypress
  $('body').on('click', '.artistManage table.tracks tr input[type="checkbox"]', onTrackFieldBlur);
  
  function handleFiles($table, files) {
    $(files).each(function (fileIndex, file) {
      if (file.type.substr(0, 6) !== 'audio/') {
        alert('File does not seem to be an audio file, and cannot be uploaded:\n' + file.name);
        return;
      }

      var titleGuess = file.name
        .replace(/\.[^/.]+$/, '') // Remove file name extension
        .replace(/\_/g, ' ') // Convert underscores to spaces
        .replace(/^.*\ \-/, '') // Attempt to remove artist name when in the format of "Artist - Title"
        .replace(/\b[0-9]{1,2}\.?\b/g, '') // Track numbers, or things that look like them
        .replace(/^[\ \-]+|[\ \-]+$/, '') // Trim, but with spaces AND hyphens
        .replace(/\ +/, ' ') // Collapse multiple spaces into one
        
      if (titleGuess.substr(-5).toUpperCase() === ', THE') {
        titleGuess = 'The ' + titleGuess.substr(0, titleGuess.length - 5);
      }
      
      // TODO: normalize case   http://stackoverflow.com/a/32485600/362536
      
      // If we failed by accidentally nuking everything, just revert to whatever the file name was in the first place and let the user figure it out
      if (!titleGuess) {
        titleGuess = file.name;
      }
      
      apiClient.post('albums', $table.attr('data-album-id'), 'tracks', {
        title: titleGuess,
        allowDownload: true,
        allowRelease: true
      }).then(function (track) {
        var $uploadProgress = $('<progress>').attr('max', file.size).val(0);
        $table.append(
          $('<tr>').attr('data-track-id', track.id).append([
            $('<td>').append(
              $('<input type="text" name="title">').val(track.title)
            ),
            $(' \
              <td> \
                  <label> \
                  <input name="allowDownload" type="checkbox" value="true" checked="checked"{{/allowDownload}} /> \
                  Download \
                </label> \
              </td> \
              <td> \
                <label> \
                  <input name="allowRelease" type="checkbox" value="true" checked="checked"{{/allowRelease}} /> \
                  Release \
                </label> \
              </td> \
            '),
            $('<td class="actions">').append($uploadProgress)
          ])
        );

        apiClient.get('tracks', track.id, 'uploadFormData').then(function (uploadFormData) {
          var dfd = $.Deferred();
          var xhr = new XMLHttpRequest();
          xhr.onerror = function (e) {
            dfd.rejectWith(xhr, e);
          };

          xhr.upload.addEventListener('progress', function (e) {
            // handle notifications about upload progress: e.loaded / e.total
            dfd.notifyWith(xhr, [e]);
          }, false);

          xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
              if (xhr.status >= 200 && xhr.status <= 299) {
                dfd.resolveWith(xhr, [xhr.status]);
              } else {
                dfd.rejectWith(xhr, [xhr.status]);
              }
            }
          };

          var formData = new FormData();
          $.each(uploadFormData.formFields, function (key, value) {
            formData.append(key, value);
          });
          formData.append('file', file, file.name);

          xhr.open('POST', uploadFormData.url, true);
          xhr.send(formData);
          
          dfd.then(function () {
            return apiClient.put('tracks', track.id, {
              state: 'Uploaded'
            });
          });

          return dfd.promise();
          
        }).progress(function (e) {
          $uploadProgress.val(e.loaded);
          
        }).then(function () {
          $uploadProgress.removeAttr('value');
          return pollForTrackStatus(track.id);
        
        }).then(function () {
          $uploadProgress.replaceWith('Upload Complete!');

        }).fail(function () {
          $uploadProgress.replaceWith('Upload failed! Please try again.');
          
        });
        
      });
      
    });
  }
  
  $('body').on('dragenter dragover drop', function (e) {
    e.preventDefault();
  });
  
  $('body').on('drop', '.artistManage table.tracks', function (e) {
    e.preventDefault();
    handleFiles($(this), e.originalEvent.dataTransfer.files);
  });
  
  $('footer .vote').hide();
  
  $(window).scroll(function (){
    $('#sidebar').height($('#main').height());
  })
  
  $('body').on('click', '[data-href]', function (e) {
    window.location.href = $(this).attr('data-href');
  });
  
  $('body').on('click', '[data-action]', function (e) {
    var $this = $(this);
    var action = $this.attr('data-action');

    // Bypass this click for elements that are children and have their own actions and browser default behavior
    if (
      e.currentTarget !== e.target && 
      !$(e.target).attr('data-action') &&
      $(e.target).is('a') // Add more elements here, as needed
    ) {
      return;
    }

    e.preventDefault();

    switch (action) {
    case 'upload':
      $('<input type="file" multiple>').on('change', function () {
        handleFiles($this.closest('table'), this.files);
      }).click();
      
      break;
    
    case 'edit':
      var newValue = prompt($this.attr('data-action-edit-prompt'), $this.text());
      if (!newValue) {
        return;
      }
      var data = {};
      data[$this.attr('data-action-edit-property')] = newValue;
      apiClient.put(
        $this.attr('data-action-edit-path'),
        $this.attr('data-action-edit-id'),
        data
      ).then(function () {
        $this.text(newValue);
      }).fail(function () {
        alert('Could not make update.  Please try again at another time or contact us for assistance.'); // TODO: We should really clean up these error handlers.
      });
      break;
      
    case 'addArtist':
      var name = prompt('What is the artist\'s or band\'s name?');
      if (name) {
        apiClient.post('artists', {
          name: name
        }).then(function () {
          utils.triggerHashBang();
        }).fail(function () {
          alert('Could not create new artist.  Please try again at another time or contact us for assistance.');
        });
      }
      break;
      
    case 'addAlbum': 
      var title = prompt('What is the title of your new album?');
      if (title) {
        var artistId = parseInt($this.attr('data-action-add-album-artist-id'));
        apiClient.post('artists', artistId, 'albums', {
          title: title
        }).then(function () {
          utils.triggerHashBang();
        }).fail(function () {
          alert('Could not create new album.  Please try again at another time or contact us for assistance.');
        });
      }
      break;
      
    case 'deleteAlbum':
      var albumId = parseInt($this.attr('data-action-delete-album-id'));
      var $albumTable = $('table[data-album-id="' + albumId + '"]');
      if (confirm(
        'Are you sure you wish to delete this album?\n\n' +  
        $this.attr('data-action-delete-album-title') + '\n\n' +
        'All tracks will also be deleted.'
      )) {
        apiClient.delete('albums', albumId).then(function () {
          $albumTable.remove();
        }).fail(function () {
          alert('Could not delete album.  Please try again at another time or contact us for assistance.');
        });
      }
      break;
      
    case 'delete':
      var trackId = parseInt($this.attr('data-action-delete-id'));
      var $row = $('tr[data-track-id="' + trackId + '"]');
      if (confirm(
        'Are you sure you wish to delete this track?\n\n' + $this.attr('data-action-delete-title')
      )) {
        apiClient.delete('tracks', trackId).then(function () {
          $row.remove();
        }).fail(function () {
          alert('Could not delete track.  Please try again at another time or contact us for assistance.');
        });
      }
      break;
    
    case 'play':
      if  (!$(e.target).is('input')) { 
        player.play(
          parseInt($this.attr('data-action-play-id')),
          $this.attr('data-action-play-type')
        );
      }

      break;
    
    case 'resume':
    case 'pause':
      player[action]();
      break;
      
    case 'voteUp':
    case 'voteDown':
      var currentUser = user.getCurrent();
      if (!currentUser) {
        window.location.href = user.getLoginUrl();
      } else {
        var mediaId = $this.closest('[data-media-id]').attr('data-media-id');
        $.ajax({
          data : JSON.stringify({
            isUpVote: (action === 'voteUp')
          }),
          contentType : 'application/json',
          method: 'PUT',
          url: config.apiRoot + '/votes/tracks/' + encodeURIComponent(mediaId),
          xhrFields: {
            withCredentials: true
          }
        }).then(function (data) {
          player.loadVotes(mediaId)
        });
      }
      break;
      
    default:
      throw new Error('Unexpected data action type.');
    }
  });
});
