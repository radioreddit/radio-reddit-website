#!/usr/bin/env bash

apt-get update

echo "Installing Nginx"
apt-get install -y nginx
chmod 755 /var/log/nginx

sed -i 's/sendfile on;/sendfile off;/' /etc/nginx/nginx.conf

cat <<EOT >> /etc/nginx/sites-available/www.dev.radioreddit.com
server {
  listen 80;

  root /vagrant/www;
  index index.html index.htm;

  server_name www.dev.radioreddit.com;

  location / {
    try_files \$uri \$uri/ =404;
  }
}
EOT

ln -s /etc/nginx/sites-available/www.dev.radioreddit.com /etc/nginx/sites-enabled/www.dev.radioreddit.com
service nginx restart

echo "Installing Node.js"
curl -s https://nodejs.org/dist/v4.4.4/node-v4.4.4-linux-x64.tar.gz | tar xzf - --strip-components=1 --directory /usr/local

echo "Installing Browserify"
npm install -g browserify

echo "Installing UglifyJS2"
npm install -g uglify-js
