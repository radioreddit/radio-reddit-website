# Radio Reddit Website
The actual site code for http://radioreddit.com.

This connects to the [Radio Reddit API](https://bitbucket.org/radioreddit/radio-reddit-api).

*Notice: This project is no longer maintained.*

## Getting Started
Copy `/www/scripts/conf/config.json.orig` to `config.json` and edit with your API server hostname.

Run `npm install` to install dependencies.

You should use [Browserify](http://browserify.org/) and [UglifyJS](https://github.com/mishoo/UglifyJS2) to build the 
client-side JS.  Once those are installed, to actually run the build:

```
npm run build
```

## Vagrant Box
A Vagrant box is attached to this repo to make it easy for you to test with.  It doesn't do much.  It has an Nginx
instance for serving files, and that's about it.

The [vagrant-hostmanager plugin](https://github.com/smdahlen/vagrant-hostmanager) is required.  To install it:

```
vagrant plugin install vagrant-hostmanager
```

Once you have that, you can simply run:

```
vagrant up
```

## License
Copyright 2016, Radio Reddit, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.